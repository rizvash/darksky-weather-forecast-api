//
//  ErrorManager.swift
//  WeatherDarkskyForecast
//
//  Created by ILLYA Rizvash on 15/12/2019.
//  Copyright © 2019 ILLYA Rizvash. All rights reserved.
//

import Foundation

// Add uniq character "RIZ" for prevent namespace error
public let RIZNetworkingErrorDomain = "riz.WeatherDarkskyForecast.NetworkingError"
//1xx Informational
public let MissingHTTPResponseError = 100
public let UnexpectedResponseError = 200
