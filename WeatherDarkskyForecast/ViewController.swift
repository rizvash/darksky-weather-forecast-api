//
//  ViewController.swift
//  WeatherDarkskyForecast
//
//  Created by ILLYA Rizvash on 11/12/2019.
//  Copyright © 2019 ILLYA Rizvash. All rights reserved.
//
// data source "https://api.darksky.net/forecast/005ca5dcd3ecb4b832a0882a1542b539/37.8267,-122.4233"

import UIKit
import CoreLocation


class ViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var appearentTempertureLabel: UILabel!
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // Outlet for: Buttoon will not avaliaable during request
    
    @IBOutlet weak var refreshButton: UIButton!
    @IBAction func refreshButtonTapped(_ sender: UIButton) {
        toggleActivityIndicator(on: true)
        getCurrentWeatherData()
    }
    
    func toggleActivityIndicator(on: Bool) {
        refreshButton.isHidden = on
        
        if on {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    func updateUIWith(currentWeather: CurrentWeather) {
        self.imageView.image = currentWeather.icon
        //self.pressureLabel.text = String(currentWeather.pressure)
        self.pressureLabel.text = currentWeather.pressureString
        self.temperatureLabel.text = currentWeather.temperatureString
        self.appearentTempertureLabel.text = currentWeather.appearentTempertureString
        self.humidityLabel.text = currentWeather.humidityString
        
    }
    let dskey = "005ca5dcd3ecb4b832a0882a1542b539"
    lazy var weatherManager = APIWeatherManager(apiKey: dskey)
    let coordinates = Coordinates(latitude: 32.090095, longitude: 34.774945)
    // TelAviv 32.090095, 34.774945
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        getCurrentWeatherData()
    }
    // ToDO
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last! as CLLocation
        
        print("my location latitude: \(userLocation.coordinate.latitude), longitude: \(userLocation.coordinate.longitude)")
    }
    
    func getCurrentWeatherData() {
        
        weatherManager.fetchCurrentWeatherWiith(coordinates: coordinates) { (result) in
            self.toggleActivityIndicator(on: false)
            switch result {
            case.Success(let currentWeather):
                self.updateUIWith(currentWeather: currentWeather)
            case.Failure(let error as NSError):
                
                let alertController = UIAlertController (title: "Unable to get data", message: "\(error.localizedDescription)", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            default: break
                
            }
        }
    }
}





