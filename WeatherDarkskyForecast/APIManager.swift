//
//  APIManager.swift
//  WeatherDarkskyForecast
//
//  Created by ILLYA Rizvash on 15/12/2019.
//  Copyright © 2019 ILLYA Rizvash. All rights reserved.
//

import Foundation

typealias JSONTask = URLSessionDataTask
typealias JASONCompletionHandler = ([String: AnyObject]?, HTTPURLResponse?, Error?) -> Void

protocol JSONDecodable {
    init?(JSON: [String: AnyObject])
}


protocol FinalURLPoint {
    var baseURL: URL { get }
    var path: String { get }
    var request: URLRequest { get }
}


enum APIResult<T> {
    case Success(T)
    case Failure(Error)
}

protocol APIManager {
    var sessionConfiguration: URLSessionConfiguration { get }
    var session: URLSession { get }
    // In Swift, closures are non-escaping by default. For escaping closures do use @escaping.
    func JSONTaskWith (request: URLRequest, completionHandler: @escaping JASONCompletionHandler) -> JSONTask
    func fetch<T: JSONDecodable>(request: URLRequest, parse: @escaping ([String: AnyObject]) -> T?, completionHandler: @escaping (APIResult<T>) -> Void)
    
    // init (sessionConfiguration: URLSessionConfiguration)
}


extension APIManager {
    func JSONTaskWith (request: URLRequest, completionHandler: @escaping JASONCompletionHandler) -> JSONTask {
        
        let dataTask = session.dataTask(with: request) {(data, response, error) in
            
            guard let HTTPResponse = response as? HTTPURLResponse else {
                
                let userInfo = [
                    NSLocalizedDescriptionKey:NSLocalizedString("Missing HTTP Response", comment: "No comments")
                ]
                let error = NSError(domain: RIZNetworkingErrorDomain, code: 100, userInfo: userInfo)
                
                completionHandler(nil, nil, error)
                return
            }
            
            if data == nil {
                if let error = error {
                    completionHandler(nil, HTTPResponse, error)
                }
            } else {
                switch HTTPResponse.statusCode {
                case 200:
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options:[]) as? [String: AnyObject]
                        completionHandler(json, HTTPResponse, nil)
                    } catch let error as NSError {
                        completionHandler(nil, HTTPResponse, error)
                    }
                    
                default:
                    print("Have got response stsus \(HTTPResponse.statusCode)")
                }
            }
        }
        return dataTask
    }
    
    func fetch<T>(request: URLRequest, parse: @escaping ([String: AnyObject]) -> T?, completionHandler: @escaping (APIResult<T>) -> Void) {
        
        let dataTask  = JSONTaskWith(request: request) { (json, response, error) in
            
            DispatchQueue.main.async ( execute: {
                guard let json = json else {
                    if let error = error {
                        completionHandler(APIResult.Failure(error))
                    }
                    return
                }
                
                if let value = parse(json) {
                    completionHandler(APIResult.Success(value))
                } else {
                    let error = NSError(domain: RIZNetworkingErrorDomain, code: 200, userInfo: nil)
                    completionHandler(APIResult.Failure(error))
                }
            })
            
        }
        dataTask.resume()
    }
}
